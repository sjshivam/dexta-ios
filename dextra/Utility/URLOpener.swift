//
//  URLOpener.swift
//  dextra
//
//  Created by Shivam Jaiswal on 23/02/19.
//  Copyright © 2019 Shivam Jaiswal. All rights reserved.
//

import Foundation
import UIKit

struct URLOpener {
    static func open(url: URL) {
        if UIApplication.shared.canOpenURL(url){
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }
    }
}
