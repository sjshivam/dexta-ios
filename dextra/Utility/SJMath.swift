//
//  SJMath.swift
//  dextra
//
//  Created by Shivam Jaiswal on 23/02/19.
//  Copyright © 2019 Shivam Jaiswal. All rights reserved.
//

import Foundation
struct SJMath {
    static func height<T: FloatingPoint>(forWidth width : T, ratio: T) -> T {
        return round(width / ratio) 
    }
}
