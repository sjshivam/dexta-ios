//
//  DataManager.swift
//  dextra
//
//  Created by Shivam Jaiswal on 23/02/19.
//  Copyright © 2019 Shivam Jaiswal. All rights reserved.
//

import Foundation

class DataManager {
    
    private static let sharedDataManager: DataManager = {
        let dataManager = DataManager()
        return dataManager
    }()
    
    class func shared() -> DataManager {
        return sharedDataManager
    }
    
    func fetchExploreData(completion: @escaping ([Explore]?, Error?) -> Void){
        
        NetworkManager.shared().createGETRequest(path: EndPoint.explrore, parameters: [:]) { (response, error) in
            
            if let error = error{
                completion(nil, error)
                return
            }
            
            guard let response = response?["explore"] as? [[String : Any]] else{
                completion(nil, SJError.defaultError)
                return
            }
            
            var explpore = [Explore]()
            for item in response{
                guard let exploreModel = Explore(data: item) else {continue}
                explpore.append(exploreModel)
            }
            completion(explpore, nil)
        }
    }
    
    func fetchUsers(userId: String, next: String? = nil, completion: @escaping (UserMeta?, Error?) -> Void){
        
        var params = [String : Any]()
        var path = EndPoint.user
        
        if let next = next {
            path += next
        }else{
            params = ["dexId" : userId]
        }
        
        NetworkManager.shared().createGETRequest(path: path, parameters: params) { (response, error) in
            
            if let error = error{
                completion(nil, error)
                return
            }
            
            guard let response = response as? [String : Any] else{
                completion(nil, SJError.defaultError)
                return
            }
            
            completion(UserMeta.init(data: response), nil)
        }
    }
    
    // TODO: via Codable
    func fetchData<Model: Codable>(of type: Model.Type,
                                   path: String,
                                   parameters: [String : Any] = [:],
                                   completion: @escaping ([Model]?, Error?) -> Void){
        
        NetworkManager.shared().createGETRequest(path: path, parameters: parameters) { (response, error) in
            
            if let error = error{
                completion(nil, error)
                return
            }
            
            guard let response = response,
                let jsonData = try? JSONSerialization.data(withJSONObject: response, options: .prettyPrinted),
                let exploreInfo = try? JSONDecoder().decode([Model].self, from: jsonData) else {
                    completion(nil, SJError.defaultError)
                    return
            }
            
            completion(exploreInfo, nil)
        }
    }
}

