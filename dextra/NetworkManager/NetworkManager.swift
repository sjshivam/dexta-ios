//
//  NetworkManager.swift
//  dextra
//
//  Created by Shivam Jaiswal on 23/02/19.
//  Copyright © 2019 Shivam Jaiswal. All rights reserved.
//

import Foundation
import Alamofire
import AlamofireNetworkActivityIndicator

struct EndPoint {
    private init() {}
    static let explrore = "explore"
    static let user = "explore/user"
}

class NetworkManager {
    
    private static let sharedNetworkManager: NetworkManager = {
        NetworkActivityIndicatorManager.shared.isEnabled = true
        let networkManager = NetworkManager()
        return networkManager
    }()
    
    class func shared() -> NetworkManager {
        return sharedNetworkManager
    }
    
    // MARK: - Properties
    private static let baseURL = "https://valakt.dextra.com/"
    
    
    // With Alamofire
    func createGETRequest(path: String, parameters: [String : Any], completion : @escaping ([AnyHashable : Any]?, Error? ) -> Void)  {
        
        guard let url = URL(string: NetworkManager.baseURL + path ) else {
            completion(nil, SJError.defaultError)
            return
        }
        
        Alamofire.request(url, method: .get, parameters: parameters)
            .validate()
            .responseJSON { [weak self] response in self?.processResponse(response, completion: completion) }
    }
    
    func createPOSTRequest(path: String, parameters: [String : Any], completion : @escaping ([AnyHashable : Any]?, Error? ) -> Void)  {
        
        guard let url = URL(string: NetworkManager.baseURL + path ) else {
            completion(nil, SJError.defaultError)
            return
        }
        
        Alamofire.request(url, method: .post, parameters: parameters)
            .validate()
            .responseJSON { [weak self] response in self?.processResponse(response, completion: completion) }
    }
    
    private func processResponse(_ response: DataResponse<Any>, completion : @escaping ([AnyHashable : Any]?, Error? ) -> Void)  {
        guard response.result.isSuccess else {
            completion(nil, response.result.error)
            return
        }
        
        guard let responseJSON = response.result.value as? [AnyHashable: Any] else{
            completion(nil, SJError.defaultError)
            return
        }
        completion(responseJSON, nil)
    }
}

enum SJError: Error {
    case defaultError
}

extension SJError: LocalizedError {
    public var errorDescription: String? {
        switch self {
        case .defaultError:
            return NSLocalizedString("Oops! Something went wrong!!", comment: "")
        }
    }
}


