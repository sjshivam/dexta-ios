//
//  UserMeta.swift
//  dextra
//
//  Created by Shivam Jaiswal on 23/02/19.
//  Copyright © 2019 Shivam Jaiswal. All rights reserved.
//

import Foundation

class UserMeta: Parser {
    let next: String?
    private (set) var users: [UserEntity]

    required init(data: [AnyHashable : Any]) {
        self.next = data["next"] as? String
        self.users = (data["users"] as? [[String : Any]] ?? []).map{UserEntity.init(data: $0)}
    }
    
    func appendItems(_ items: [UserEntity]){
        self.users = items + self.users
    }
}
