//
//  UserViewModel.swift
//  dextra
//
//  Created by Shivam Jaiswal on 23/02/19.
//  Copyright © 2019 Shivam Jaiswal. All rights reserved.
//

import UIKit

class UserViewModel {

    private var users = [UserEntity]()
    var userId = ""
    var next: String?
    var userMeata: UserMeta?
    
    func fetchUsers(completion: @escaping (Bool, Error?) -> Void){
        DataManager.shared().fetchUsers(userId: userId, next: next) { [weak self](response, error) in
            self?.processResponse(response: response, error: error, completion: completion)
        }
    }
    
    private func processResponse(response: UserMeta?, error: Error?, completion: @escaping (Bool, Error?) -> Void)
    {
        guard let response = response else {
            completion(false, error ?? SJError.defaultError)
            return
        }
        
        if userMeata == nil{
            userMeata = response
        }else{
            response.appendItems(userMeata!.users)
            userMeata = response
        }
        
        print("count = \(response.users.count)")
        response.users.forEach({ (repo) in
            print(repo.firstName + repo.lastName)
        })
        
        next = response.next
        completion(true, nil)
    }
    
    func numberOfSections() -> Int {
        return 1
    }
    
    func numberOfRows() -> Int {
        return userMeata?.users.count ?? 0
    }
    
    func itemAtIndexPath(_ indexPath: IndexPath) -> UserEntity {
        guard let meata = userMeata else { fatalError() }
        return meata.users[indexPath.row]
    }
    
    func shouldLoadNextPage(for indexPath: IndexPath, collectionView: UICollectionView) -> Bool {
        guard let meta = userMeata, let _ = meta.next else { return false }
        return collectionView.isLastCell(for: indexPath)
    }
}
