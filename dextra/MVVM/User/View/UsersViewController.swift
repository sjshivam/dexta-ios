//
//  UsersViewController.swift
//  dextra
//
//  Created by Shivam Jaiswal on 23/02/19.
//  Copyright © 2019 Shivam Jaiswal. All rights reserved.
//

import UIKit

fileprivate struct PaddingConstants {
    static let cellInBetweenPadding: CGFloat = 10
    static let cellVerticalPadding: CGFloat = 10
    static let cellSidePadding: CGFloat = 10
    static let sectionInset: UIEdgeInsets = UIEdgeInsets(top: 0, left: cellSidePadding, bottom: 0, right: cellSidePadding)
}

class UsersViewController: UIViewController {
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    let viewModel = UserViewModel()
    var userId: String = ""
    var name: String = ""
    
    var columnLayout = SJColumnFlowLayout.init(cellsPerRow: 2, minimumInteritemSpacing: PaddingConstants.cellInBetweenPadding, minimumLineSpacing: PaddingConstants.cellVerticalPadding, sectionInset: PaddingConstants.sectionInset){
        didSet{
            columnLayout.scrollDirection = .vertical
            collectionView.collectionViewLayout = columnLayout
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        columnLayout.scrollDirection = .vertical
        collectionView.collectionViewLayout = columnLayout
        collectionView.contentInsetAdjustmentBehavior = .always
        collectionView.delegate = self;
        collectionView.dataSource = self;
        self.title = name
        viewModel.userId = userId
        downloadData()
    }
    
    func downloadData(){
        viewModel.fetchUsers { [weak self] (fetched, error) in
            if let error = error{
                self?.showAlert(message: error.localizedDescription)
            }else if self?.viewModel.numberOfRows() == 0{
                self?.showAlert(message: "No users!")
            }else{
                self?.collectionView.reloadData()
            }
        }
    }
}

extension UsersViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout
{
    // MARK: collectionView
    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel.numberOfRows()
    }
    
    public func numberOfSections(in collectionView: UICollectionView) -> Int {
        return viewModel.numberOfSections()
    }
    
    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "UserCollectionViewCell", for: indexPath) as! UserCollectionViewCell
        cell.entity = viewModel.itemAtIndexPath(indexPath)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if viewModel.shouldLoadNextPage(for: indexPath, collectionView: collectionView){
            downloadData()
        }
    }
}

extension UICollectionView{
    
    func isFirstCell(for indexpath: IndexPath) -> Bool {
        return ((indexpath.section == 0) && (indexpath.row == 0))
    }
    
    func isLastCell(for indexpath: IndexPath) -> Bool {
        return (self.numberOfItems(inSection: indexpath.section) - 1 ) == indexpath.row
    }
}

class SJColumnFlowLayout: UICollectionViewFlowLayout {
    
    let cellsPerRow: Int
    
    init(cellsPerRow: Int, minimumInteritemSpacing: CGFloat = 0, minimumLineSpacing: CGFloat = 0, sectionInset: UIEdgeInsets = .zero) {
        self.cellsPerRow = cellsPerRow
        super.init()
        
        self.minimumInteritemSpacing = minimumInteritemSpacing
        self.minimumLineSpacing = minimumLineSpacing
        self.sectionInset = sectionInset
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func prepare() {
        super.prepare()
        
        guard let collectionView = collectionView else { return }
        let marginsAndInsets = sectionInset.left + sectionInset.right + collectionView.safeAreaInsets.left + collectionView.safeAreaInsets.right + minimumInteritemSpacing * CGFloat(cellsPerRow - 1)
        let itemWidth = ((collectionView.bounds.size.width - marginsAndInsets) / CGFloat(cellsPerRow)).rounded(.down)
        itemSize = CGSize(width: itemWidth, height: itemWidth)
    }
    
    override func invalidationContext(forBoundsChange newBounds: CGRect) -> UICollectionViewLayoutInvalidationContext {
        let context = super.invalidationContext(forBoundsChange: newBounds) as! UICollectionViewFlowLayoutInvalidationContext
        context.invalidateFlowLayoutDelegateMetrics = newBounds.size != collectionView?.bounds.size
        return context
    }
    
}
