//
//  ViewController.swift
//  dextra
//
//  Created by Shivam Jaiswal on 23/02/19.
//  Copyright © 2019 Shivam Jaiswal. All rights reserved.
//

import UIKit
import SafariServices

class HomeViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    let viewModel = ExploreViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.delegate = self
        self.tableView.dataSource = self
        downloadData()
    }
    
    func downloadData(){
        viewModel.fetchExploreData { [weak self] (fetched, error) in
            self?.tableView.reloadData()
            if let error = error{
                self?.showAlert(message: error.localizedDescription)
            }
        }
    }
}

extension UIViewController
{
    func showAlert(message: String?) {
        let alert = UIAlertController(title: nil, message: message, preferredStyle: .alert)
        let confirmAction = UIAlertAction(title: "Okay", style: .default) {(_) in }
        alert.addAction(confirmAction)
        present(alert, animated: true, completion: nil)
    }
}

extension HomeViewController: UITableViewDelegate, UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.numberOfRows()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return viewModel.numberOfSections()
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return viewModel.titleForHeaderInSection(section: section)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return viewModel.tableView(tableView, heightForRowAt: indexPath)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "exploreCell", for: indexPath) as! ExploreTableViewCell
        cell.delegate = self
        let exploreItem = viewModel.itemAtIndexPath(indexPath)
        cell.exploreItem = exploreItem
        return cell
    }
}

extension HomeViewController: ExploreTableViewCellDelegate
{
    func exploreCell(_ cell: ExploreTableViewCell, didSelectEntity entity: Entity) {
        
        guard let exploreItem = cell.exploreItem else { return }
        
        switch exploreItem.sectionType {
        case .banner:
            guard let webURL = URL.init(string: entity.actionUrl ) else { return }
            let config = SFSafariViewController.Configuration()
            let vc = SFSafariViewController(url: webURL, configuration: config)
            present(vc, animated: true)
        case .dex:
            guard let entity = entity as? DexEntity else { return }
            let controller = self.storyboard?.instantiateViewController(withIdentifier: "UsersViewController") as! UsersViewController
            controller.userId = entity.id_dex
            controller.name = entity.name
            self.navigationController?.pushViewController(controller, animated: true)
        default:
            break
        }
        
    }
}
