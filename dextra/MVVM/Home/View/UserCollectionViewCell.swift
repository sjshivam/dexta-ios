//
//  UserCollectionViewCell.swift
//  dextra
//
//  Created by Shivam Jaiswal on 23/02/19.
//  Copyright © 2019 Shivam Jaiswal. All rights reserved.
//

import UIKit

class UserCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var clapsView: UIButton!
    @IBOutlet weak var handleLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    
    var entity: UserEntity? {
        didSet{
            layoutUpdate()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setup()
    }
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        imageView.layer.cornerRadius = imageView.bounds.height/2.0
    }
    
    func setup() {
        imageView.clipsToBounds = true
    }
    
    func layoutUpdate(){
        guard let item = entity else { return }
        nameLabel.text = item.firstName + item.lastName
        handleLabel.text = item.handle
        clapsView.setTitle(String(item.totalProjectClaps), for: .normal)
        
        guard let url = URL(string: item.imageUrl) else{
            imageView.image = #imageLiteral(resourceName: "placeholder")
            return
        }
        
        imageView.af_setImage(
            withURL: url,
            placeholderImage: #imageLiteral(resourceName: "placeholder"),
            filter: nil,
            imageTransition: .crossDissolve(0.2)
        )
    }
}
