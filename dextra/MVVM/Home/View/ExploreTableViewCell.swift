//
//  ExploreTableViewCell.swift
//  dextra
//
//  Created by Shivam Jaiswal on 23/02/19.
//  Copyright © 2019 Shivam Jaiswal. All rights reserved.
//

import UIKit

protocol ExploreTableViewCellDelegate: AnyObject {
    func exploreCell(_ cell: ExploreTableViewCell, didSelectEntity entity: Entity)
}

fileprivate struct Constants{
    static let requirementPadding: CGFloat = 40
    static let dexWidth: CGFloat = 150
    static let userWidth: CGFloat = 150
}

class ExploreTableViewCell: UITableViewCell {
    @IBOutlet weak var collectionView: UICollectionView!
    
    let layout = UICollectionViewFlowLayout()
    
    weak var delegate: ExploreTableViewCellDelegate?
    var exploreItem: Explore? = nil {
        didSet{
            layoutSetup()
            collectionView.reloadData()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setup()
    }
    
    func setup(){
        layout.scrollDirection = .horizontal
        layout.minimumInteritemSpacing = 0
        layout.minimumLineSpacing = 0
        layout.sectionInset = .zero
        collectionView.collectionViewLayout = layout
        collectionView.contentInsetAdjustmentBehavior = .always
        collectionView.delegate = self;
        collectionView.dataSource = self;
        collectionView.showsHorizontalScrollIndicator = false
    }
    
    func layoutSetup()
    {
        guard let item = exploreItem else { return }
        switch item.sectionType {
        case .banner:
            collectionView.isPagingEnabled = true
            layout.minimumInteritemSpacing = 0
            layout.minimumLineSpacing = 0
        default:
            collectionView.isPagingEnabled = false
            layout.minimumInteritemSpacing = 4.0
            layout.minimumLineSpacing = 4.0
        }
    }
    
    static func reuseId() -> String {
        return "ExploreTableViewCell"
    }
}

extension ExploreTableViewCell: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout
{
    // MARK: collectionView
    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return exploreItem?.entities.count ?? 0
    }
    
    public func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let exploreItem = exploreItem else { fatalError()}
        let item = exploreItem.entities[indexPath.row]
        switch exploreItem.sectionType {
        case .user:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "UserCollectionViewCell", for: indexPath) as! UserCollectionViewCell
            cell.entity = item as? UserEntity
            return cell
        default:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "exploreCollectionCell", for: indexPath) as! ExploreCollectionViewCell
            cell.updateCell(urlString: item.imageUrl, title: item.name)
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let item = exploreItem?.entities[indexPath.row] else { return }
        delegate?.exploreCell(self, didSelectEntity: item)
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize{
        
        guard let item = exploreItem else { return .zero }
        let width = ExploreTableViewCell.trimmedWidth(width: collectionView.bounds.size.width, explore: item)
        return CGSize.init(width: width, height: collectionView.bounds.size.height)
    }
    
    static func trimmedWidth(width: CGFloat, explore: Explore) -> CGFloat
    {
        var width = width

        switch explore.sectionType {
        case .requirement:
            width -= (explore.entities.count == 1) ? 0 : Constants.requirementPadding
        case .dex:
            width = Constants.dexWidth
        case .user:
            width = Constants.userWidth
        default:
            width -= 0
        }
        
        return width
    }
    
    static func heightForWidth(width: CGFloat, explore: Explore) -> CGFloat {
        let width = ExploreTableViewCell.trimmedWidth(width: width, explore: explore)
        let height = SJMath.height(forWidth: width, ratio: explore.sectionType.aspectRatio())
        return height
    }
}

