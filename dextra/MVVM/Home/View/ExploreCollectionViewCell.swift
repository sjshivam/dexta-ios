//
//  ExploreCollectionViewCell.swift
//  dextra
//
//  Created by Shivam Jaiswal on 23/02/19.
//  Copyright © 2019 Shivam Jaiswal. All rights reserved.
//

import UIKit
import AlamofireImage

class ExploreCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    
    static func reuseId() -> String {
        return "ExploreCollectionViewCell"
    }
    
    func updateCell(urlString: String, title: String?){
        titleLabel.text = title
        
        guard let url = URL(string: urlString) else{
            imageView.image = #imageLiteral(resourceName: "placeholder")
            return
        }
        
        imageView.af_setImage(
            withURL: url,
            placeholderImage: #imageLiteral(resourceName: "placeholder"),
            filter: nil,
            imageTransition: .crossDissolve(0.2)
        )
    }
}
