//
//  ExploreViewModel.swift
//  dextra
//
//  Created by Shivam Jaiswal on 23/02/19.
//  Copyright © 2019 Shivam Jaiswal. All rights reserved.
//

import UIKit

class ExploreViewModel {

    private var explore = [Explore]()
    
    func fetchExploreData(completion: @escaping (Bool, Error?) -> Void){
        DataManager.shared().fetchExploreData { [weak self] (explore, error) in
            self?.processResponse(response: explore, error: error, completion: completion)
        }
    }
    
    private func processResponse(response: [Explore]?, error: Error?, completion: @escaping (Bool, Error?) -> Void)
    {
        guard let response = response else {
            completion(false, error ?? SJError.defaultError)
            return
        }
        explore = response
        completion(true, nil)
    }
    
    func numberOfSections() -> Int {
        return explore.count
    }
    
    func numberOfRows() -> Int {
        return 1
    }
    
    func titleForHeaderInSection(section: Int) -> String? {
        return explore[section].title
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let exploreItem = explore[indexPath.section]
        let height = ExploreTableViewCell.heightForWidth(width: tableView.bounds.size.width, explore: exploreItem)
        return height
    }
    
    func itemAtIndexPath(_ indexPath: IndexPath) -> Explore {
        return explore[indexPath.section]

    }
    
}
