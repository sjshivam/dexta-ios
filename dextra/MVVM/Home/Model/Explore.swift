//
//  Explore.swift
//  dextra
//
//  Created by Shivam Jaiswal on 23/02/19.
//  Copyright © 2019 Shivam Jaiswal. All rights reserved.
//

import Foundation
import UIKit

protocol Parser {
    init?(data: [AnyHashable : Any])
}

enum SectionType: String, Codable {
    case dex, user, requirement, banner
    
    func aspectRatio() -> CGFloat {
        switch self {
        case .banner:
            return 1584.0/552.0
        case .user:
            return 140.0 / 180.0
        default:
            return 1.0
        }
    }
}

class Explore: Parser, Codable {
    let title: String
    let sectionType: SectionType
    let entities : [Entity]
    
    required init?(data: [AnyHashable : Any]) {
        
        guard let title = data["title"] as? String,
            let type = SectionType.init(rawValue: data["sectionType"] as? String ?? ""),
            let entities = data["entities"] as? [[String : Any]], !entities.isEmpty
            else { return nil }
        self.title = title
        self.sectionType = type
    
        self.entities = entities.map{Explore.classType(type).init(data: $0)}
    }
    
    static func classType(_ type: SectionType) -> Entity.Type {
        switch type {
        case .user:
            return UserEntity.self
        case .dex:
            return DexEntity.self
        default:
            return Entity.self
        }
    }
}

class Entity: Parser, Codable {
    let imageUrl: String
    let actionUrl: String
    let name: String

    required init(data: [AnyHashable : Any]) {
        imageUrl = data["imageUrl"] as? String ?? ""
        actionUrl = data["actionUrl"] as? String ?? ""
        name = data["name"] as? String ?? ""
    }
}

class DexEntity: Entity {
    let id_dex: String
    
    required init(data: [AnyHashable : Any]) {
        id_dex = data["_id"] as? String ?? ""
        super.init(data: data)
    }
    
    required init(from decoder: Decoder) throws {
        fatalError("init(from:) has not been implemented")
    }
}

class UserEntity: Entity {
    
    let firstName: String
    let lastName: String
    let handle: String
    let totalProjectClaps: Int
    
    required init(data: [AnyHashable : Any]) {
        firstName = data["firstName"] as? String ?? ""
        lastName = data["lastName"] as? String ?? ""
        handle = data["handle"] as? String ?? ""
        totalProjectClaps = data["totalProjectClaps"] as? Int ?? 0
        super.init(data: data)
    }
    
    required init(from decoder: Decoder) throws {
        fatalError("init(from:) has not been implemented")
    }
}
